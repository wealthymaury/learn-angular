angular.module('myApp')
	.provider('greet', function () {
		//configuracion para antes de crear el servicio
		//para configurarlo hay que usar greetProvider que tiene acceso a esta parte de las funciones
		//la confugracion de un Provider se hace desde config
		this.greeting = 'Hello'; //valor por defecto
		
		this.setGreeting = function(greeting){
			this.greeting = greeting;
		}

		//con esto se configura y se crea lo que sera el servicio greet,
		//el servicio es la funcion que se retorna
		this.$get = function(){
			var greeting = this.greeting;
			
			return function(name){
				alert(greeting + ', ' + name);
			}
		}
	})
	.factory('weatherService', function ($http) {
		return {
			getWeather: function (city, country) {
				var query = city + ',' + country;
				return $http.get('http://api.openweathermap.org/data/2.5/weather', {
					params: {
						q: query
					},
					cache: true //almacena cache mientras no refresques la pagina
				}).then(function (response) {
					return response.data.weather[0].description;
				});
			}
		}
	})
	//trabajando con interceptores
	.factory('customInterceptor', function ($q) {
		return {
			request: function (config) {
				// es llamado antes de hacer la peticion
				// puedes modificar la configuracion aqui
				// puedes retornar una promesa
				return config;
			},
			requestError: function (rejectionReason) {
				// si el intereptor anterior tuvo error se llama este, aqui puedes cambiar la configuracion
				// e intentar recuperarte del error retornando una nueva configuracion
				if(ableToRecover(rejectionReason)){
					// nueva config
					return config;
				}else {
					return $q.reject('Could not recover'); //una promesa siemmpre rechazada
				}
			},
			response: function (response) {
				// este interceptor se llama cuando se recibe la respuesta de backend
				// aqui puedes modificar la repsuesta

				return response;
			},
			responseError: function (rejectionReason) {
				// si el interceptor anterior retorna una promesa rechazada se llama este
				if(ableToRecover(rejectionReason)){
					// nueva response o promesa
					return response;
				}else {
					return $q.reject('Could not recover'); //una promesa siemmpre rechazada
				}
			}
 
		};
	})
	//creando una clase para usar un recurso
	.factory('Entry', function ($resource) {
		return $resource('/api/entries/:id');
	});



