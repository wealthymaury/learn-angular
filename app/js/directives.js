angular.module('myApp.directives', [])
.directive('helloWorld', function () {
	return {
		// usando esto declaramos que el scope sera aislado
		scope: {
			message: '=messageAttr', // atributo proveniente del padre
			showMessage: '&showMessageAttr' //funcion proveniente del padre
		},
		restrict: 'AEC',
		replace: true,
		transclude: 'element', // or true
		template: '<p ng-click="clearMessage()">Hello World! {{ message }} <span ng-transclude></span></p>', //aqui en ng-transclude se pone el contenido
		compile: function (tElem, attrs) {
			// haz alguna transformacion del DOM aqui
			// esta funcion la usa ng-repeat
			// return function (scope, elem, attrs) {
				
			// };
			return this.link;
		},
		link: function (scope, elem, attrs, ctrl, transclude) {
			scope.$watch('message', function (value) {
				//console.log('Message changed!');
				scope.showMessage({arg: 'haha'});
			});

			scope.clearMessage = function () {
				scope.message = '';
			}

			elem.bind('mouseover', function () {
				elem.css('cursor', 'pointer');
			});

			transclude(function (clone){
				clone.css('background-color', 'yellow');
				elem.after(clone);
			});
		}
	}
});

angular.module('testApp.directives', [])
.directive('shakeDiv', function ($animate, $timeout){
	return {
		restrict: 'AEC',
		replace: true,
		link: function(scope, elem, attrs){
			scope.generateRandom = function(){
				scope.randomValue = Math.random().toFixed(2);
				if(scope.randomValue > 0.5){
					$animate.addClass(elem.find('.my-animation-class-tres'), 'shake');
					$timeout(function(){
						$animate.removeClass(elem.find('.my-animation-class-tres'), 'shake');
					}, 1000);
				}
			}
		},
		template: '<div><input type="button" ng-click="generateRandom()" value="Generate random value"><h2 class="my-animation-class-tres">{{ randomValue }}</h2></div>'
	}
});





