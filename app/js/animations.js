angular.module('testApp.animations', ['ngAnimate'])
.animation('.my-animation-class', function(){
	return {
		enter: function (elem, done){
			elem.css({
				background: 'green',
				opacity: 0
			});

			elem.animate({
				backgroundColor: 'white',
				opacity: 1
			}, done);

		},
		leave: function (elem, done){
			elem.css({
				opacity: 1
			});

			elem.animate({
				opacity: 0
			}, done)
		},
		move: function (elem, done){
			done();
		},
		beforeAddClass: function (element, className, done){
			done();
		},
		addClass: function (element, className, done){
			done();
		},
		beforeRemoveClass: function (element, className, done){
			done();
		},
		removeClass: function (element, className, done){
			done();
		}
	}
})
.animation('.my-animation-class-dos', function(){
	return {
		addClass: function(elem, className, done){
			if(className === 'growing-div'){
				elem.animate({
					width: '200px',
					height: '200px',
					duration: 1000,
					queue: true
				}, done);
			}
		},
		removeClass: function(elem, className, done){
			if(className === 'growing-div'){
				elem.animate({
					width: '100px',
					height: '100px',
					duration: 1000,
					queue: true
				}, done);
			}
		}
	}
});