angular.module('mainApp', [
  'loginModule',
  'commentModule',
]);

//requires
angular.module('myApp', [
	'myApp.controllers',
	'myApp.directives',
	//'ngRoute' //dependencia para poder usar rutas
	'ui.router',
	'ngResource'
]);

angular.module('testApp', [
	'testApp.directives',
	'testApp.animations',
	'ngAnimate',
]);

//configuracion de rutas y vistas usando ngRoute normal
// angular.module('myApp').config(function ($routeProvider) {
// 	$routeProvider.when('/view1', {templateUrl: 'partials/view1.html', controller: 'Controller1'});
// 	$routeProvider.when('/view2/:firstname/:lastname', {
// 		templateUrl: 'partials/view2.html', 
// 		controller: 'Controller2',
// 		//para pasar dependencias adicionales, que pueden ser sacadas por ajax o algo
// 		resolve: {
// 			names: function () {
// 				return ['Misko', 'Vojta', 'Brad'];
// 			}
// 		}
// 	});
// 	//en este controller se usa un ng-template en lugar de un partial
// 	$routeProvider.when('/view3', {templateUrl: '/view3.tpl', controller: 'Controller3'});
// 	$routeProvider.otherwise({redirectTo: '/view1'});
// 	//$locationProvider.html5Mode(true); //para quitar el # de la url, hay que inyectarlo como $routeProvider
// });

//confguracion de rutas usando estados y angular UI router
angular.module('myApp').config(function ($stateProvider, $urlRouterProvider, $locationProvider, greetProvider, $provide, $httpProvider) {
	$stateProvider
		.state('view1', {
			url: '/view1',
			controller: 'Controller1',
			templateUrl: 'partials/view1.html'
		})
		.state('view2', {
			url: '/view2/:firstname/:lastname',
			controller: 'Controller2',
			resolve: {
				names: function () {
					return ['Misko', 'Vojta', 'Brad'];
				}
			},
			templateUrl: 'partials/view2.html'
		});
	$urlRouterProvider.otherwise('/view1');
	//$locationProvider.html5Mode(true);
	
	//es un proveedor personalizado mio XD
	greetProvider.setGreeting('haha hola');

	//con este decorador modificamos el comportamiento de un servicio y le agregamos cosas
	//$delegate es el servicio original, en este caso $log
	$provide.decorator('$log', function ($delegate) {
		$delegate.postInfoToUrl = function (message) {
			//do post to URL
			$delegate.log('Data sended');
		}

		return $delegate; //retornar servicio adaptado
	});

	//con estas configuraciones es posible alterar las peticiones
	$httpProvider.defaults.transformRequest = function (data, getHeaders) {
		var headers = getHeaders();
		headers['Content-Type'] = 'text/plain';

		return JSON.stringify(data);
	}

});

//callback
angular.module('myApp').run(function ($rootScope) {
	$rootScope.title = 'Famous books';
	$rootScope.name = 'Root Scope';
	//eventos de rutas
	$rootScope.$on('$routeChangeStart', function () {
		//debugger;
	});
	$rootScope.$on('$routeChangeSuccess', function () {
		//debugger;
	});
});