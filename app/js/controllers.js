angular.module('myApp.controllers', [])
	// herencia de scopes
	.controller('SiteController', function ($scope) {
		$scope.publisher = 'Site point';
		$scope.type = 'Web development';
		$scope.name = 'Scope for SiteController';
	})
	// watchers
	.controller('BookController', function ($scope) {
		$scope.books = ['Jump start HTML5', 'Jump start CSS', 'Jump start Responsive Web Design'];
		$scope.name = 'Scope for BookController';
		$scope.addToWishList = function(book){
			$scope.wishListCount++;
		};
		$scope.wishListCount = 0;
		// $watch me permite escuchar cambios en wishListCount
		// unbindwatcher es una funcion que me permite dejar de escuchar
		var unbindWatcher = $scope.$watch('wishListCount', function (newValue, oldValue) {
			if(newValue != oldValue)
			{
				console.log('called '+newValue+' times');
				if(newValue == 2){
					alert('Great!');
					unbindWatcher();
				}
			}
		}, true);
	})
	// $apply
	.controller('TimeoutController', function ($scope, $timeout) {
		$scope.scheduleTask = function() {
			setTimeout(function () {
				// por alguna extraña razon, al actualizar el modelo este no aparece en el DOM
				// por ello es necesario encerrar el codigo en $apply para que force a actualizar el DOM
				$scope.$apply(function () {
					$scope.message = 'Fetched after 3 seconds';
					console.log('message: ' + $scope.message );
				});
			}, 3000);
		}
		//esta funcion hace lo mismo que la de arriba pero usando el servicio $timeout de angular
		//el servicio hay que inyectarlo y solito llama al $apply por nosotros
		$scope.fetchMessage = function () {
			$timeout(function () {
				$scope.message = 'Fetched after 3 seconds';
				console.log('message: ' + $scope.message );
			}, 3000);
		}
	})
	// events $emit and $broadcast
	.controller('MessageController', function ($scope, $timeout) {
		$scope.messages = [{
			sender: 'user1', 
			text: 'message'
		}];
		var timer;
		var count = 0;
		$scope.loadMessages = function () {
			count++;
			$scope.messages.push({
				sender: 'user1',
				text: 'random message' + count
			});
			timer = $timeout($scope.loadMessages, 2000);
			if(count == 3){
				// broadcast emite del padre a los hijos
				$scope.$broadcast('EVENT_NO_DATA', 'Not connected');
				$timeout.cancel(timer);
			}
		};
		timer = $timeout($scope.loadMessages, 2000);
		$scope.$on('EVENT_RECEIVED', function () {
			console.log('Receiver emmited event');
		});
	})
	.controller('StatsController', function ($scope) {
		$scope.name = 'World';
		$scope.status = 'Connected';
		$scope.statusColor = 'green';
		$scope.$on('EVENT_NO_DATA', function (event, data) {
			console.log('receiver broadcast event');
			$scope.status = data;
			$scope.statusColor = 'red';
			// emit emite del hijo a los padres
			$scope.$emit('EVENT_RECEIVED');
		});
	})
	//routing
	.controller('Controller1', function ($scope, $location, $state, greet, $log) {
		greet('maury');
		$scope.message = 'Hello world';
		$scope.loadView2 = function () {
			//para cuando usas ngRoute
			//$location.path('/view2/' + $scope.firstname + '/' + $scope.lastname);

			//para cuando usas estados en UI router, el objeto son los parametros de la URL
			$state.go('view2', {
				firstname: $scope.firstname,
				lastname: $scope.lastname
			});
		}
		//esta es una funcion del servicio $log que agregue con un decorator desde config
		$log.postInfoToUrl('algo');
	})
	.controller('Controller2', function ($scope, $stateParams, names) { //$routeParams
		$scope.now = new Date();
		// $scope.firstname = $routeParams.firstname;
		// $scope.lastname = $routeParams.lastname;
		$scope.firstname = $stateParams.firstname;
		$scope.lastname = $stateParams.lastname;
		$scope.names = names;
	})
	.controller('Controller3', function ($scope) {
		//
	})
	.controller('UserController', function ($scope) {
		$scope.user = {};
		$scope.$watch('user', function (newValue, oldValue) {
			//debugger;
            console.log(newValue);
		}, true);
	})
	// promesas
	.controller('MainController', ['$scope', '$q', '$interval', function ($scope, $q, $interval) {
		$scope.getPromise = function () {
			var i = 0;
			var deferred = $q.defer();
			
			var timer = $interval(function () {
				if(!! $scope.cancelRequested) {
					deferred.reject('Promise rejected');
					$interval.cancel(timer);
				}

				i = i + 1;

				if(i == 5) {
					deferred.resolve('Counter has reached 5');
					$interval.cancel(timer);
				}else {
					deferred.notify('Counter has reacher ' + i);
				}
			}, 1000);

			return deferred.promise;
		}

		$scope.getAsyncMessage = function () {
			var promise = $scope.getPromise();

			promise.then(function (message) {
				$scope.status = "Resolved: " + message;
			}, function (message) {
				$scope.status = "Rejected: " + message;
			}, function (message) {
				$scope.status = "Notifying: " + message;
			});
		}
	}])
	.controller('weatherController', function ($scope, weatherService) {
		$scope.getWeather = function () {
			$scope.weatherDescription = "Fetching . . .";

			weatherService.getWeather($scope.city, $scope.country)
				.then(function (data) {
					$scope.weatherDescription = data;
				}, function () {
					$scope.weatherDescription = "Could not obtain data";
				});
			
		}
	})
	//uso de recursos
	.controller('ResourceController', function ($scope, Entry) {
		var entry = Entry.get({ id: $scope.id }, function() {
			console.log(entry);
		}); // returns a single entry
		// es importante que puedes usar la variable entry como si fuera sincrona, porque cuando la informacion
		// llege del server el databinding hara el resto, no habra problema

		var entries = Entry.query(function (){
			console.log(entries);
		}); // returns all the entries

		Entry.save($scope.entry, function () {
			// saving
		});

		Entry.delete({ id: $scope.id }, function () {
			// Deleted from server
		});
		//remove and delete es lo mismo, pero es mas compatible remove

		// manipulando una instancia
		$scope.entry = new Entry();
		$scope.entry.data = 'some data';
	})
	.controller('someController', function ($scope) {
		$scope.message = 'I love AngularJS';
		$scope.showMessage = function (arg) {
			console.log('Mesage changed ' + arg);
		}
	});


